<?php
/**
* @package Scheduled_Announcements_Widget
* @version 0.1.5
*/
/*
Plugin Name: Scheduled Announcements Widget
Plugin URI: http://nlb-creations.com/2012/02/01/wordpress-plugin-scheduled-announcement-widget/
Description: This plugin created a set of scheduled announcements that can be displayed as a widget or with shortcode.
Author: Nikki Blight <nblight@nlb-creations.com>
Version: 0.1.5
Author URI: http://www.nlb-creations.com
*/

//add the Settings link to the plugin page
function saw_action_links( $links, $file ) {
	if ( $file != plugin_basename( __FILE__ ))
	return $links;

	$settings_link = '<a href="edit.php?post_type=sched_announcement&page=scheduled-announcement-widget">' . __( 'Settings', 'scheduled-announcements-widget' ) . '</a>';

	array_unshift( $links, $settings_link );

	return $links;
}
add_filter( 'plugin_action_links', 'saw_action_links',10,2);

//jQuery MUST be available, so just in case the theme doesn't use it, enqueue it now
function saw_scripts() {
	wp_enqueue_script( 'jquery' );
}
add_action( 'init', 'saw_scripts' );

//there are some known conflicts with the Event Planner plugin on the settings page, so let's dump the javascript and styles for that plugin if it's installed.
function saw_remove_conflicts() {
	if(stristr($_SERVER['REQUEST_URI'], "wp-admin/edit.php?post_type=sched_announcement")) {
		wp_dequeue_script('epl-event-manager-js');
		wp_dequeue_script('epl-forms-js');
		wp_dequeue_script('events_planner_js');
		wp_dequeue_style('events-planner-jquery-ui-style');
		wp_dequeue_style('events-planner-stylesheet');
	}
}
add_action('admin_init', 'saw_remove_conflicts');

add_action( 'init', 'saw_create_post_types' );
add_action('init', 'saw_create_taxonomy');

//create a custom post type to hold custom data
function saw_create_post_types() {
	register_post_type( 'sched_announcement',
	array(
			'labels' => array(
				'name' => __( 'Announcements' ),
				'singular_name' => __( 'Announcement' ),
				'add_new' => __( 'Add Announcement'),
				'add_new_item' => __( 'Add Announcement'),
				'edit_item' => __( 'Edit Announcement' ),
				'new_item' => __( 'New Announcement' ),
				'view_item' => __( 'View Announcement' )
	),
			'show_ui' => true,
			'description' => 'Post type for Announcements',
			'menu_position' => 5,
			'menu_icon' => WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)) . '/saw-menu-icon.png',
			'public' => true,
			'taxonomies' => array('saw_categories'),
			'supports' => array('title', 'editor', 'page-attributes'),
			'can_export' => true
	)
	);
}

//create custom taxonomies for the new post types
function saw_create_taxonomy() {
	register_taxonomy('saw_categories', array('sched_announcement'),
	array(
			'hierarchical' => true, 
			'label' => 'Categories', 
			'singular_label' => 'Announcement Category',
			'public' => true,
			'show_tagcloud' => false,
			'query_var' => true
		)
	);
}

// Add a custom postmeta field for the redirect url
add_action( 'add_meta_boxes', 'saw_dynamic_add_custom_box' );

//save the data in the custom field
add_action( 'save_post', 'saw_dynamic_save_postdata' );

//Add boxes to the edit screens for a qrcode post type
function saw_dynamic_add_custom_box() {
	
	add_meta_box(
		'dynamic_saw_dates',
	__( 'Dates (optional)', 'myplugin_textdomain' ),
		'saw_dates_custom_box',
		'sched_announcement',
		'side');
}

//print the custom meta box
function saw_dates_custom_box() {
	global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'dynamicMetaDate_noncename' );
    
    echo '<div id="meta_inner">';

    //get the saved metadata
    $start = get_post_meta($post->ID,'saw_start_date',true);
    $end = get_post_meta($post->ID,'saw_end_date',true);
    
    $set = array();
    if($start) {
    	$set['start'] = explode("-", $start);
    }
    
    if($end) {
    	$set['end'] = explode("-", $end);
    }
    
    $year = date('Y');
    ?>
    <em>Both dates must be set to use scheduling feature.</em><br /><br />
    <strong>Start Date: </strong>
    <select name="saw_start_mon">
    	<option value=""></option>
    	<option value="01" <?php if(isset($set['start']) && $set['start'][1] == '01') { echo 'selected="selected"'; } ?>>Jan</option>
    	<option value="02" <?php if(isset($set['start']) && $set['start'][1] == '02') { echo 'selected="selected"'; } ?>>Feb</option>
    	<option value="03" <?php if(isset($set['start']) && $set['start'][1] == '03') { echo 'selected="selected"'; } ?>>Mar</option>
    	<option value="04" <?php if(isset($set['start']) && $set['start'][1] == '04') { echo 'selected="selected"'; } ?>>Apr</option>
    	<option value="05" <?php if(isset($set['start']) && $set['start'][1] == '05') { echo 'selected="selected"'; } ?>>May</option>
    	<option value="06" <?php if(isset($set['start']) && $set['start'][1] == '06') { echo 'selected="selected"'; } ?>>Jun</option>
    	<option value="07" <?php if(isset($set['start']) && $set['start'][1] == '07') { echo 'selected="selected"'; } ?>>Jul</option>
    	<option value="08" <?php if(isset($set['start']) && $set['start'][1] == '08') { echo 'selected="selected"'; } ?>>Aug</option>
    	<option value="09" <?php if(isset($set['start']) && $set['start'][1] == '09') { echo 'selected="selected"'; } ?>>Sep</option>
    	<option value="10" <?php if(isset($set['start']) && $set['start'][1] == '10') { echo 'selected="selected"'; } ?>>Oct</option>
    	<option value="11" <?php if(isset($set['start']) && $set['start'][1] == '11') { echo 'selected="selected"'; } ?>>Nov</option>
    	<option value="12" <?php if(isset($set['start']) && $set['start'][1] == '12') { echo 'selected="selected"'; } ?>>Dec</option>
    </select>
    
    <select name="saw_start_day">
        <option value=""></option>
        <?php
        	for($i=1; $i<=31; $i++) { 
        		$index = $i;
        		if($i < 10) {
        			$index = '0'.$i;
        		}
        		
        		echo '<option value="'.$index.'" ';
        		if(isset($set['start']) && $set['start'][2] == $index) {
					echo 'selected="selected"';
				}
				echo '>'.$i.'</option>';
			}
         ?>
	</select>
    
    <select name="saw_start_year">
    	<option value=""></option>
    	<option value="<?php echo $year-1; ?>" <?php if(isset($set['start']) && $set['start'][0] == $year-1) { echo 'selected="selected"'; } ?>><?php echo $year-1; ?></option>
    	<option value="<?php echo $year; ?>" <?php if(isset($set['start']) && $set['start'][0] == $year) { echo 'selected="selected"'; } ?>><?php echo $year; ?></option>
    	<option value="<?php echo $year+1; ?>" <?php if(isset($set['start']) && $set['start'][0] == $year+1) { echo 'selected="selected"'; } ?>><?php echo $year+1; ?></option>
    	<option value="<?php echo $year+2; ?>" <?php if(isset($set['start']) && $set['start'][0] == $year+2) { echo 'selected="selected"'; } ?>><?php echo $year+2; ?></option>
    	<option value="<?php echo $year+3; ?>" <?php if(isset($set['start']) && $set['start'][0] == $year+3) { echo 'selected="selected"'; } ?>><?php echo $year+3; ?></option>
    	<option value="<?php echo $year+4; ?>" <?php if(isset($set['start']) && $set['start'][0] == $year+4) { echo 'selected="selected"'; } ?>><?php echo $year+4; ?></option>
    </select>
    
    <br />
    
    <strong>End Date:&nbsp;&nbsp; </strong>
    <select name="saw_end_mon">
		<option value=""></option>
    	<option value="01" <?php if(isset($set['end']) && $set['end'][1] == '01') { echo 'selected="selected"'; } ?>>Jan</option>
        <option value="02" <?php if(isset($set['end']) && $set['end'][1] == '02') { echo 'selected="selected"'; } ?>>Feb</option>
       	<option value="03" <?php if(isset($set['end']) && $set['end'][1] == '03') { echo 'selected="selected"'; } ?>>Mar</option>
       	<option value="04" <?php if(isset($set['end']) && $set['end'][1] == '04') { echo 'selected="selected"'; } ?>>Apr</option>
       	<option value="05" <?php if(isset($set['end']) && $set['end'][1] == '05') { echo 'selected="selected"'; } ?>>May</option>
       	<option value="06" <?php if(isset($set['end']) && $set['end'][1] == '06') { echo 'selected="selected"'; } ?>>Jun</option>
       	<option value="07" <?php if(isset($set['end']) && $set['end'][1] == '07') { echo 'selected="selected"'; } ?>>Jul</option>
       	<option value="08" <?php if(isset($set['end']) && $set['end'][1] == '08') { echo 'selected="selected"'; } ?>>Aug</option>
       	<option value="09" <?php if(isset($set['end']) && $set['end'][1] == '09') { echo 'selected="selected"'; } ?>>Sep</option>
       	<option value="10" <?php if(isset($set['end']) && $set['end'][1] == '10') { echo 'selected="selected"'; } ?>>Oct</option>
       	<option value="11" <?php if(isset($set['end']) && $set['end'][1] == '11') { echo 'selected="selected"'; } ?>>Nov</option>
       	<option value="12" <?php if(isset($set['end']) && $set['end'][1] == '12') { echo 'selected="selected"'; } ?>>Dec</option>
	</select>
        
	<select name="saw_end_day">
        <option value=""></option>
        <?php
        	for($i=1; $i<=31; $i++) { 
        		$index = $i;
        		if($i < 10) {
        			$index = '0'.$i;
        		}
        		
        		echo '<option value="'.$index.'" ';
        		if(isset($set['end']) && $set['end'][2] == $index) {
					echo 'selected="selected"';
				}
				echo '>'.$i.'</option>';
			}
         ?>
	</select>
        
	<select name="saw_end_year">
        <option value=""></option>
        <option value="<?php echo $year-1; ?>" <?php if(isset($set['end']) && $set['end'][0] == $year-1) { echo 'selected="selected"'; } ?>><?php echo $year-1; ?></option>
        <option value="<?php echo $year; ?>" <?php if(isset($set['end']) && $set['end'][0] == $year) { echo 'selected="selected"'; } ?>><?php echo $year; ?></option>
        <option value="<?php echo $year+1; ?>" <?php if(isset($set['end']) && $set['end'][0] == $year+1) { echo 'selected="selected"'; } ?>><?php echo $year+1; ?></option>
        <option value="<?php echo $year+2; ?>" <?php if(isset($set['end']) && $set['end'][0] == $year+2) { echo 'selected="selected"'; } ?>><?php echo $year+2; ?></option>
        <option value="<?php echo $year+3; ?>" <?php if(isset($set['end']) && $set['end'][0] == $year+3) { echo 'selected="selected"'; } ?>><?php echo $year+3; ?></option>
        <option value="<?php echo $year+4; ?>" <?php if(isset($set['end']) && $set['end'][0] == $year+4) { echo 'selected="selected"'; } ?>><?php echo $year+4; ?></option>
	</select>
    
    <?php 
	echo '</div>';
}

//when the post is saved, save our custom postmeta too
function saw_dynamic_save_postdata( $post_id ) {
	//if our form has not been submitted, we dont want to do anything
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// verify this came from the our screen and with proper authorization
	if (isset($_POST['dynamicMetaDate_noncename'])){
		if ( !wp_verify_nonce( $_POST['dynamicMetaDate_noncename'], plugin_basename( __FILE__ ) ) )
		return;
	}
	else {
		return;
	}
	//save the data
	$start = '';
	$end = '';
	
	if($_POST['saw_start_year'] != '') {
		$start = $_POST['saw_start_year'].'-'.$_POST['saw_start_mon'].'-'.$_POST['saw_start_day'];
	}
	
	if($_POST['saw_end_year'] != '') {
		$end = $_POST['saw_end_year'].'-'.$_POST['saw_end_mon'].'-'.$_POST['saw_end_day'];
	}

	update_post_meta($post_id,'saw_start_date',$start);
	update_post_meta($post_id,'saw_end_date',$end);
}

//get the info for the announcement set
function saw_get_current($order = 'ASC', $tax = '') {

	global $wpdb;

	//get the current date
	$now = date('Y-m-d', strtotime(current_time("mysql")));

	//fetch and announcements scheduled between the start and end dates or that have no date
	$start_ids = $wpdb->get_results("SELECT `meta`.`post_id` FROM ".$wpdb->prefix."postmeta AS `meta`
											WHERE `meta_key` = 'saw_start_date' 
											AND TO_DAYS(`meta_value`) <= TO_DAYS('".$now."');", OBJECT_K);
	
	$end_ids = $wpdb->get_results("SELECT `meta`.`post_id` FROM ".$wpdb->prefix."postmeta AS `meta`
											WHERE `meta_key` = 'saw_end_date' 
											AND TO_DAYS(`meta_value`) >= TO_DAYS('".$now."');", OBJECT_K);
	
	$no_date_ids = $wpdb->get_results("SELECT `meta`.`post_id` FROM ".$wpdb->prefix."postmeta AS `meta`
												WHERE `meta_key` = 'saw_start_date' 
												AND `meta_value` = '';", OBJECT_K);
	
	$saw_ids = array_intersect(array_keys($start_ids), array_keys($end_ids));
	$saw_ids = array_unique(array_merge($saw_ids, array_keys($no_date_ids)));
	
	
	
	//if a category is specified, filter out anything not in that taxonomy
	if($tax != '' && $tax != 0) {
		$tax_ids = $wpdb->get_results("SELECT `posts`.`ID` FROM ".$wpdb->prefix."posts AS `posts`
											JOIN ".$wpdb->prefix."term_relationships AS `terms`
											ON `posts`.`ID` = `terms`.`object_id` 
											WHERE `terms`.`term_taxonomy_id` = ".$tax.";", OBJECT_K);
		
		
		$saw_ids = array_intersect(array_keys($tax_ids), $saw_ids);
		
	}
	
	$saw_ids = implode(",", $saw_ids);
	
	//query for the currently scheduled announcements
	$announcements = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts AS `posts` WHERE `posts`.`ID` IN (".$saw_ids.") AND `posts`.`post_status` = 'publish' ORDER BY `posts`.`menu_order` ".$order.";");
	
	return $announcements;
}

//shortcode function for use in theme
function saw_show_widget($atts) {
	extract( shortcode_atts( array(
		'title' => '',	
		'show_titles' => 1,
		'scroll' => '',
		'speed' => '',
		'transition' => '',
		'width' => '',
		'height' => '',
		'link' => '',
		'text' => '',
		'order' => '',
		'saw_id' => 'saw_ticker_custom',
		'tax' => ''
	), $atts ) );
	
	//if settings not specified in the short code, use the option settings
	$scroll_settings = $scroll;
	if($scroll == '') {
		$scroll_settings = get_option('saw_scroll_options');
	}
	
	$speed_settings = $speed;
	if($speed == '') {
		$speed_settings = get_option('saw_speed_options');
	}
	
	$trans_settings = $transition;
	if($trans_settings == '') {
		$trans_settings = get_option('saw_trans_options');
	}
	
	$width_settings = $width;
	if($width == '') {
		$width_settings = get_option('saw_width_options');
	}
	
	$height_settings = $height;
	if($height == '') {
		$height_settings = get_option('saw_height_options');
	}
	
	$text_settings = $text;
	if($text == '') {
		$text_settings = get_option('saw_text_color_options');
	}
	
	$link_settings = $link;
	if($link == '') {
		$link_settings = get_option('saw_link_color_options');
	}
	
	$order_settings = $order;
	if($order == '') {
		$order_settings = get_option('saw_order_options');
	}
	
	//fetch the current announcememts
	$announcements = saw_get_current($order_settings, $tax);
	
	$output = '';
	if(count($announcements) > 0) {
		$output .= '<style type="text/css">';
		$output .= 'div.saw_announcements_'.$saw_id.' {';
		$output .= '	width: '.$width_settings.'px;';
		$output .= '	overflow: hidden;';
		$output .= '}';
					
		$output .= 'div.saw_announcements_'.$saw_id.'.horizontal ul#'.$saw_id.' {';
		$output .= '    width: '.($width_settings*2).'px;';
		$output .= '    height: '.$height_settings.'px;';
		$output .= '    overflow: hidden;';
		$output .= '	margin: 0px;';
		$output .= '}';
					
		$output .= 'div.saw_announcements_'.$saw_id.'.vertical ul#'.$saw_id.' {';
		$output .= '	width: '.$width_settings.'px;';
		$output .= '	height: '.$height_settings.'px;';
		$output .= '	overflow: hidden;';
		$output .= '	margin: 0px;';
		$output .= '}';
					 
		$output .= 'ul#'.$saw_id.' li {';
		$output .= '	width: '.$width_settings.'px;';
		$output .= '	height: '.$height_settings.'px;';
		$output .= '	padding: 0px;';
		$output .= '	display: block;';
		$output .= '	float: left;';
		$output .= '}';
					 
		$output .= 'ul#'.$saw_id.' li a {';
		$output .= '	color: #'.$link_settings.';';
		$output .= '}';
					
		$output .= 'ul#'.$saw_id.' li span {';
		$output .= '	display: inline;';
		$output .= '	color: #'.$text_settings.';';
		$output .= '}';
					
		$output .= '.saw_title {';
		$output .= '	font-weight: bold;';
		$output .= '}';
		$output .= '</style>';
		
		
		$output .= '<div class="saw_announcements_'.$saw_id.' '.$scroll_settings.'">';
		$rand = rand();
		if(count($announcements) > 1) {
			
			$output .= '<script type="text/javascript">';
			$output .= 'jQuery(function()';
			$output .= '{';
			$output .= '	var ticker = function()';
			$output .= '	{';
			$output .= "		jQuery('#".$saw_id." li:first').animate( {";
			
			if($scroll_settings == 'horizontal') {
				$output .= "marginLeft: '-".$width_settings."px'";
			}
			if($scroll_settings == 'vertical') {
				$output .= "marginTop: '-".$height_settings."px'";
			}
			
			$output .= '	}, '.$trans_settings.', function()';
			$output .= '			{';
			$output .= "				jQuery(this).detach().appendTo('ul#".$saw_id."').removeAttr('style');";
			$output .= '			});';
			$output .= '	interval;';
			$output .= '	};';
			$output .= '	var interval = setInterval(ticker, '.$speed_settings.');';
			$output .= "	jQuery('#".$saw_id." li:first').hover(function() {";
			$output .= '		clearInterval(interval);';
			$output .= '	}, function() {';
			$output .= "		interval = setInterval(ticker, ".$speed_settings.");";
			$output .= '		});';
			$output .= '	interval;';
			$output .= '});';
			$output .= '</script>';
		}
		
		if($title != '') {
			$output .= '<h2 class="saw_list_title">'.$title.'</h2>';
		}
		$output .= '	<ul id="'.$saw_id.'">';
	 
		foreach($announcements as $msg) {
			$output .= '<li class="saw_msg">';
			if($show_titles) {
				$output .= '<span class="saw_title">'.$msg->post_title.':</span> ';
			}
			$output .= '<span class="saw_content">'.apply_filters('the_content', $msg->post_content).'</span>';
			$output .= '</li>';
		}
		
		$output .= '	</ul>';
		$output .= '</div>';
	}
	return $output;
}
add_shortcode( 'announcements', 'saw_show_widget');


/* Sidebar widget functions */
class SAW_Widget extends WP_Widget {

	function SAW_Widget() {
		$widget_ops = array('classname' => 'SAW_Widget', 'description' => 'Currently scheduled announements');
		$this->WP_Widget('SAW_Widget', 'Scheduled Announcements', $widget_ops);
	}

	function form($instance) {
		$instance = wp_parse_args((array) $instance, array( 'title' => '', 'show_tax' => '' ));
		$title = $instance['title'];
		$show_titles = $instance['show_titles'];
		$show_tax = $instance['show_tax'];

		?>
			<p>
		  		<label for="<?php echo $this->get_field_id('title'); ?>">Title: 
		  		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		  		</label>
		  	</p>
		  	
		  	<p>
		  		<label for="<?php echo $this->get_field_id('show_titles'); ?>"> 
		  		<input id="<?php echo $this->get_field_id('show_titles'); ?>" name="<?php echo $this->get_field_name('show_titles'); ?>" type="checkbox" <?php if($show_titles) { echo 'checked="checked"'; } ?> /> 
		  		Show Announcment Titles in Widget
		  		</label>
		  	</p>
		  	
		  	<p>
		  		<label>Show announcements only in:</label><br />
		  		<?php
		  			$args = array('name' => $this->get_field_name('show_tax'),
									'id' => $this->get_field_id('show_tax'),
    								'taxonomy' => 'saw_categories',
		  							'show_option_all' => 'All categories',
		  							'selected' => esc_attr($show_tax)); 
		  			wp_dropdown_categories( $args );
		  		?>
		  		
			</p>
		<?php
	}
 
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['show_titles'] = ( isset( $new_instance['show_titles'] ) ? 1 : 0 );
		$instance['show_tax'] = $new_instance['show_tax'];
		return $instance;
	}
 
	function widget($args, $instance) {
		extract($args, EXTR_SKIP);
 
		$title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
		$show_titles = $instance['show_titles'];
		$show_tax = $instance['show_tax'];
		
		//get widget options
		$scroll_settings = get_option('saw_scroll_options');
		if($scroll_settings == '') {
			$scroll_settings = 'horizontal';	
		}
		
		$speed_settings = get_option('saw_speed_options');
		if($speed_settings == '') {
			$speed_settings = 4000;
		}
		
		$trans_settings = get_option('saw_trans_options');
		if($trans_settings == '') {
			$trans_settings = 800;
		}
		
		$width = get_option('saw_width_options');
		if($width == '') {
			$width = 200;
		}
		
		$height = get_option('saw_height_options');
		if($height == '') {
			$height = 120;
		}
		
		$text = get_option('saw_text_color_options');
		if($text == '') {
			$text = '333333';
		}
		
		$link = get_option('saw_link_color_options');
		if($link == '') {
			$link = '0000ff';
		}
		
		$order = get_option('saw_order_options');
		if($order == '') {
			$order = 'ASC';
		}
		
 		//fetch the current announcements and assign a random id number
		$announcements = saw_get_current($order, $show_tax);
		$widget_id = rand(0,9999999);

		if(count($announcements) > 0) {
			echo $before_widget;
			?>
			<style type="text/css">
				div.saw_announcements {
					width: <?php echo $width; ?>px;
					overflow: hidden;
				}
				
				div.saw_announcements.horizontal ul#saw_ticker-<?php echo $widget_id; ?> {
				    width: <?php echo ($width)*2; ?>px;
				    height: <?php echo $height; ?>px;
				    overflow: hidden;
				    margin: 0px;
				}
				
				div.saw_announcements.vertical ul#saw_ticker-<?php echo $widget_id; ?> {
				    width: <?php echo $width; ?>px;
				    height: <?php echo $height; ?>px;
				    overflow: hidden;
				    margin: 0px;
				}
				 
				ul#saw_ticker-<?php echo $widget_id; ?>  li {
				    width: <?php echo $width; ?>px;
				    height: <?php echo $height; ?>px;
				    padding: 0px;
				    display: block;
				    float: left;
				}
				 
				ul#saw_ticker-<?php echo $widget_id; ?> li a {
				    color: #<?php echo $link; ?>;
				}
				
				ul#saw_ticker-<?php echo $widget_id; ?> li span {
				    display: inline;
				    color: #<?php echo $text; ?>;
				}
				
				.saw_title {
					font-weight: bold;
				}
			</style>
			
			<div class="widget saw_announcements <?php echo $scroll_settings; ?>">
				<?php if(count($announcements) > 1): ?>
				<script type="text/javascript">
					jQuery(function()
						{
							//animate the ticker
						    var ticker<?php echo $widget_id; ?> = function()
						    {
						        jQuery('#saw_ticker-<?php echo $widget_id; ?> li:first').animate( {<?php if($scroll_settings == 'horizontal') { echo "marginLeft: '-".$width."px'"; } if($scroll_settings == 'vertical') { echo "marginTop: '-".$height."px'"; } ?>}, <?php echo $trans_settings; ?>, function()
								{
									jQuery(this).detach().appendTo('ul#saw_ticker-<?php echo $widget_id; ?>').removeAttr('style');
								});
	
								//after the animation completes, set the interval again
								interval;
							};
	
							//set the interval for the animation
							var interval = setInterval(ticker<?php echo $widget_id; ?>, <?php echo $speed_settings; ?>);
	
							//on hover, pause the ticker, and restart it on mouseout
							jQuery('#saw_ticker-<?php echo $widget_id; ?> li:first').hover(function() {
								clearInterval(interval);
							}, function() {
								interval = setInterval(ticker<?php echo $widget_id; ?>, <?php echo $speed_settings; ?>);
							});
	
							//start the interval for the first animation
							interval;					    
						});
				</script>
				<?php endif; ?>
				<?php 
					if (!empty($title)) {
						echo $before_title . $title . $after_title;
					}
					else {
						echo $before_title.$after_title;
					}
				?>
				
				<ul id="saw_ticker-<?php echo $widget_id; ?>">
					<?php 
					
					foreach($announcements as $msg) {
						echo '<li class="saw_msg">';
						if($show_titles) {
							echo '<span class="saw_title">'.$msg->post_title.':</span> ';
						}
						
						echo '<span class="saw_content">'.apply_filters('the_content', $msg->post_content).'</span>';
						echo '</li>';
					}
					?>
				</ul>
			</div>
			<?php
 
			echo $after_widget;
		}
	}
}

/* Admin Functions */

//create a menu item for the options page
function saw_admin_menu() {
	if (function_exists('add_options_page')) {
		//add to Settings tab
		//add_options_page('Announcement Options', 'Scheduled Announcements', 'manage_options', 'scheduled-announcement-widget', 'saw_admin_options');
		
		//add to Announcements tab
		add_submenu_page( 'edit.php?post_type=sched_announcement', 'Announcement Settings', 'Settings', 'manage_options', 'scheduled-announcement-widget', 'saw_admin_options' );
	}
}
add_action( 'admin_menu', 'saw_admin_menu' );

//output the options page
function saw_admin_options() {

  	//watch for form submission
	if (!empty($_POST['saw_scroll_options'])) {
    	//validate the referrer field
		check_admin_referer('saw_options_valid');
 
		//update options settings
		update_option('saw_scroll_options', $_POST['saw_scroll_options']);
		update_option('saw_speed_options', $_POST['saw_speed_options']);
		update_option('saw_trans_options', $_POST['saw_trans_options']);
		update_option('saw_width_options', $_POST['saw_width_options']);
		update_option('saw_height_options', $_POST['saw_height_options']);
		update_option('saw_text_color_options', $_POST['saw_text_color_options']);
		update_option('saw_link_color_options', $_POST['saw_link_color_options']);
		update_option('saw_order_options', $_POST['saw_order_options']);
		
		//show success
		echo '<div id="message" class="updated fade"><p><strong>' . __('Your configuration settings have been saved.') . '</strong></p></div>';
	}
 
	//display the admin options page
?>
 
<div style="width: 620px; padding: 10px">
	<h2><?php _e('Scheduled Announcement Options'); ?></h2>
	<form action="" method="post" id="me_likey_form" accept-charset="utf-8" style="position:relative">
		
		<?php wp_nonce_field('saw_options_valid'); ?>
		
		<input type="hidden" name="action" value="update" />
		<table class="form-table">
			
			<tr valign="top">
				<th scope="row">Order</th>
				<td>
					<?php 
						$order_settings = get_option('saw_order_options');
						if($order_settings == '') {
							update_option('saw_order_options', 'ASC');
							$order_settings = 'ASC';
						}
					?>
					<select name="saw_order_options" id="saw_order_options">
						<option value="ASC" <?php if($scroll_settings == 'ASC') { echo 'selected="selected"'; } ?>>Ascending</option>
						<option value="DESC" <?php if($scroll_settings == 'DESC') { echo 'selected="selected"'; } ?>>Descending</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">&nbsp;</th>
					<td>Please select the order in which you would like announcments to scroll.</td>
			</tr>
			
			<tr valign="top">
				<th scope="row">Scroller Options</th>
				<td>
					<?php 
						$scroll_settings = get_option('saw_scroll_options');
						if($scroll_settings == '') {
							update_option('saw_scroll_options', 'horizontal');
							$scroll_settings = 'horizontal';
						}
					?>
					<select name="saw_scroll_options" id="saw_scroll_options">
						<option value="vertical" <?php if($scroll_settings == 'vertical') { echo 'selected="selected"'; } ?>>Vertical</option>
						<option value="horizontal" <?php if($scroll_settings == 'horizontal') { echo 'selected="selected"'; } ?>>Horizontal</option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">&nbsp;</th>
					<td>Please select how you want your announcements to scroll.</td>
			</tr>
			
			<tr valign="top">
				<th scope="row">Scroller Speed</th>
				<td>
					<?php 
						$speed_settings = get_option('saw_speed_options');
						if($speed_settings == '') {
							update_option('saw_speed_options', 4000);
							$speed_settings = 4000;
						}
					?>
					<input type="text" name="saw_speed_options" value="<?php echo $speed_settings; ?>" />
				</td>
			</tr>
			
			<tr valign="top">
				<th scope="row">Transition Speed</th>
				<td>
					<?php 
						$trans_settings = get_option('saw_trans_options');
						if($trans_settings == '') {
							update_option('saw_trans_options', 800);
							$trans_settings = 800;
						}
					?>
					<input type="text" name="saw_trans_options" value="<?php echo $trans_settings; ?>" />
				</td>
			</tr>
			
			<tr valign="top">
				<th scope="row">&nbsp;</th>
					<td>Enter the transition speed (speed at which the animation plays) of the scroller in milliseconds.</td>
			</tr>
			
			<tr valign="top">
				<th scope="row">Widget width</th>
				<td>
					<?php 
						$width_settings = get_option('saw_width_options');
						if($width_settings == '') {
							update_option('saw_width_options', 200);
							$width_settings = 200;
						}
					?>
					<input type="text" name="saw_width_options" value="<?php echo $width_settings; ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">&nbsp;</th>
					<td>Enter the width of the scroller in pixels.</td>
			</tr>
			
			<tr valign="top">
				<th scope="row">Widget height</th>
				<td>
					<?php 
						$height_settings = get_option('saw_height_options');
						if($height_settings == '') {
							update_option('saw_height_options', 120);
							$height_settings = 120;
						}
					?>
					<input type="text" name="saw_height_options" value="<?php echo $height_settings; ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">&nbsp;</th>
					<td>Enter the height of the scroller in pixels.</td>
			</tr>
			
			<?php $dir = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__)); ?>
			<script type="text/javascript" src="<?php echo $dir; ?>/js/jscolor/jscolor.js"></script>
			
			<tr valign="top">
				<th scope="row">Text color</th>
				<td>
					<?php 
						$text_color_settings = get_option('saw_text_color_options');
						if($text_color_settings == '') {
							update_option('saw_text_color_options', '333333');
							$text_color_settings = '333333';
						}
					?>
					
					<input type="text" id="colorField1" name="saw_text_color_options" value="<?php echo $text_color_settings; ?>" />
					
					<script type="text/javascript">
					var myPicker1 = new jscolor.color(document.getElementById('colorField1'), {});
					myPicker1.fromString('<?php echo $text_color_settings; ?>');
					</script>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">&nbsp;</th>
					<td>Choose a color for text in the widget.</td>
			</tr>
			
			<tr valign="top">
				<th scope="row">Link color</th>
				<td>
					<?php 
						$link_color_settings = get_option('saw_link_color_options');
						if($link_color_settings == '') {
							update_option('saw_link_color_options', '0000ff');
							$link_color_settings = '0000ff';
						}
					?>
					
					<input type="text" id="colorField2" name="saw_link_color_options" value="<?php echo $link_color_settings; ?>" />
					
					<script type="text/javascript">
					var myPicker2 = new jscolor.color(document.getElementById('colorField2'), {});
					myPicker2.fromString('<?php echo $link_color_settings; ?>');
					</script>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">&nbsp;</th>
					<td>Choose a color for any links in the widget.</td>
			</tr>
			
			<tr valign="top">
				<th scope="row">&nbsp;</th>
				<td>
					<input type="submit" name="Submit" class="button-primary" value="<?php _e('Save Changes') ?>"/>
				</td>
			</tr>
		</table>
	</form>
</div>
 
<?php
}

add_action( 'widgets_init', create_function('', 'return register_widget("SAW_Widget");') );

?>