
<div class="wrap">    
    <div id="icon-wpml" class="icon32" style="clear:both" ><br /></div>    
    <h2><?php _e('Support', 'sitepress') ?></h2>
    
    <p style="margin-top: 20px;">
        <?php _e('Technical support for clients is available via <a target="_blank" href="http://forum.wpml.org">WPML forum</a>.','sitepress'); ?>
    </p>

    <?php
    
    // Installer plugin active?
    $installer_on = defined('WPRC_VERSION') && WPRC_VERSION;

    $wp_plugins = get_plugins();    
    $wpml_plugins = array(
                            'WPML Multilingual CMS'         => false,
                            'WPML CMS Nav'                  => false,
                            'WPML String Translation'       => false,
                            'WPML Sticky Links'             => false,
                            'WPML Translation Management'   => false
                         );
                          
    
    foreach($wp_plugins as $plugin){
        if (isset($wpml_plugins[$plugin['Name']])) {
            $wpml_plugins[$plugin['Name']] = $plugin;
        }
    }
    unset($wp_plugins);
    
    echo '
        <table class="widefat" style="width:350px;">
            <thead>
                <tr>    
                    <th>' . __('Plugin Name', 'sitepress') . '</th>
                    <th style="text-align:right">' . __('Status', 'sitepress') . '</th>
                </tr>
            </thead>    
            <tbody>
        ';
    
    foreach($wpml_plugins as $name => $p){
        
        echo '<tr>';
        echo '<td>' . $name . '</td>';
        echo '<td align="right">';
        if(empty($p)){
            if(!$installer_on){                
                echo __('Not installed');
            }else{
                echo '<a href="' . admin_url('plugins.php?s=') . urlencode($name) . '">' . __('Download', 'sitepress') . '</a>';
            }
        }else{
            if(!$installer_on){                
                echo __('Installed');
            }else{
                echo '<a href="' . admin_url('plugins.php?s=') . urlencode($name) . '">' . __('Installed', 'sitepress') . '</a>';
            }
        } 
        echo '</td>';
        echo '</tr>';
        
    }
    echo '
            </tbody>
        </table>
    ';
        
    if(!$installer_on){
        
        echo '
            <br />
            <div class="icl_cyan_box">
                <p>' . __('The recommended way to install WPML on new sites and upgrade WPML on this site is by using our Installer plugin.', 'sitepress') . '</p>
                <br />
                <p>
                    <a class="button-primary" href="http://wp-compatibility.com/installer-plugin/">' . __('Download Installer', 'sitepress') . '</a>&nbsp;
                    <a href="http://wpml.org/faq/install-wpml/#2">' . __('Instructions', 'sitepress') . '</a>
                </p>
            </div>
        ';
        
        
    }
    ?>
    
    <p style="margin-top: 20px;">
    <?php printf(__('For advanced access or to completely uninstall WPML and remove all language information, use the <a href="%s">troubleshooting</a> page.', 'sitepress'), admin_url('admin.php?page=' . ICL_PLUGIN_FOLDER . '/menu/troubleshooting.php')); ?> 
    </p>
    
    
</div>
