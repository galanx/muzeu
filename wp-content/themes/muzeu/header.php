<!DOCTYPE html>
<!--[if IE 7 | IE 8]>
<html class="ie" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<title><?php wp_title( '|', true, 'right' );
			// Add the blog name.
		bloginfo( 'name' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";

		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Page %s', 'muzeu' ), max( $paged, $page ) );
		?>
	</title>
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
	<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
	<script src="<?php bloginfo('template_directory'); ?>/js/menu-highlight.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/js/muzeu-map.js"></script>
</head>
<body>
	<div class="wrapper">
		<div class="header">
			<a href="<?php echo get_option('home'); ?>"/><div id="logo"></div></a>
			<div class="top-links">
				<?php //wp_nav_menu( array('menu' => 'Top Menu' )); ?>
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Top Menu') ) : ?>
					<?php dynamic_sidebar('Top Menu')?>
				<?php endif; ?>
			</div>
			<div id="lang">
				<?php do_action('icl_language_selector'); ?>
			</div>
		</div>