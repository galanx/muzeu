<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Main Sidebar') ) : ?>
	<div id="eksponata">
		<?php if( class_exists( 'LenSlider' ) ) {
			LenSlider::lenslider_output_slider( '802a910dc1' );
		}?>
	</div>
	<div id="fushata">
		<?php if( class_exists( 'LenSlider' ) ) {
			LenSlider::lenslider_output_slider( '69dedc7316' );
		}?>
	</div>
	<div id="poll">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Pyetje Sidebar') ) : ?>
					<?php dynamic_sidebar( 'Pyetje Sidebar' ); ?>
		<?php endif; ?>
	</div>
<?php endif; ?>