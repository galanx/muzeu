<?php get_header(); ?>
<div class="main-content-pages">
	<div class="content-pages">
		<?php if(have_posts()) : while (have_posts()) : the_post();?>
		<h1><?php the_title(); ?></h1>	
		<?php the_content(); ?>
		<?php endwhile; endif; ?><br><br>
		<?php comments_template(); ?>
	</div>
	<div class="sidebar">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Page Sidebar') ) : ?>
			<?php dynamic_sidebar( 'Page Sidebar' ); ?>
		<?php endif; ?>
	</div>
</div>	
<?php get_footer(); ?>
