		<div class="footer">
				<div class="footer-content">
					<div id="news">
						<div id="announcements">
							<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('News Sidebar') ) : ?>
								<?php dynamic_sidebar( 'News Sidebar' ); ?>
							<?php endif; ?>
						</div>	
						<div id="social-icons">
							<a href="#"><img src="<?php bloginfo('template_directory')?>/images/facebook-icon.png"/></a>
							<a href="#"><img src="<?php bloginfo('template_directory')?>/images/twitter-icon.png"/></a>
							<a href="#"><img src="<?php bloginfo('template_directory')?>/images/google-icon.png"/></a>
						</div>
					</div>
					<div class="footer-links">
						<?php wp_nav_menu( array('menu' => 'Footer Links' )); ?>
					</div>
				</div>
			</div>
		</div>
		<?php wp_footer(); ?>
	</body>
</html>