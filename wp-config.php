<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'muzeu');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('WP_ALLOW_REPAIR', true);
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_r&+36+y@J;V#fgQPt-nrxZ&2;LzEk{lW*5w=W]K/=yLiY-<L*=%_s+TN+n)-PV?');
define('SECURE_AUTH_KEY',  'o2l:3Bn|7+P@o12lYlsXE:s/.,rBby-D+G-=a8A>4h-u>LRP}Uo0^u@%:/~!B9_z');
define('LOGGED_IN_KEY',    '=UB[rz+uIA$3IUQ#PiIPGl=Fg3DO:|5ro-gcf 7)<+8pynNfswr(dHK-uSa+oa$x');
define('NONCE_KEY',        'r 3J.pE70b6mS.0ln(C{UvJ +5VU;SmI#{~J74fA+*%2H^`6buiOls9-jb-N8ThK');
define('AUTH_SALT',        '-BKWUBD|5TNR~+e)KD!e /#|rnpkfL5buu;SxCNlTj2-sq$WP+BYh]}L4Z? gS#t');
define('SECURE_AUTH_SALT', '-e5R+|~zq8Wq]`SE9cz05wOC6$<h2T)WIZ 9D}X#snA+3l>%U5p74wn2tFj+I]6+');
define('LOGGED_IN_SALT',   'VP`@}@UXFH2t51_.a}RIrgTa9H:1CsN/;YCr92qcJ>+vb]!R-H|hW&#8gR^-9zI6');
define('NONCE_SALT',       'UO|52JDUflB*`ai4|NXkz)&r]4J.b;#qRC1|(-9/1J>b@5KaqS)Ana6QQT0!|?mq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';
/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');
define('FS_METHOD', 'direct');
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define('WP_CACHE', true);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
